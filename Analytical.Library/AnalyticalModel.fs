﻿namespace Analytical.Library

open System

type AnalyticalModel() = 
    member val Rho_pc = 0.0 with get, set
    member val Rho_p = 0.0 with get, set
    member val C_sr = 0.0 with get, set
    member val Rho_k = 0.0 with get, set
    member val Rho_pr = 0.0 with get, set
    member val Rho_di = 0.0 with get, set
    member val T_c = 0.0 with get, set
    member val T_r = 0.0 with get, set
    member val Lambda_phi1 = 0.0 with get, set
    member val Lambda_phin = 0.0 with get, set
    member val It = 0 with get, set

    member this.Run(n: int, T_0: float, T_r: float, t_k1: float, t_k2: float, c: int, t_pr: float, d: int, t_di: float, p_i: float, k1: float, k2: float, delta: float) = 
        let beta = 1.0 / (1.0 - p_i)
        let t_k = 0.5 * (t_k1 + t_k2)
        let p_di = 1.0 / float d
        let lambda_phi1 = k1 * List.min [1.0 / (2.0 * t_k); float c / (beta * t_pr); 1.0 / (beta * p_di * t_di);] * ((float n - 1.0) / float n)
        let rec loop lambda_phi1 it = 
            let T_k = (2.0 * t_k) / (1.0 - 2.0 * lambda_phi1 * t_k)
            let T_pr = (beta * t_pr) / (1.0 - (beta * lambda_phi1 * (t_pr / float c) ** float c))
            let T_di = (beta * t_di) / (1.0 - beta * p_di * lambda_phi1 * t_di)
            let lambda_phi = float (n - 1) / (T_0 + T_r + T_k + T_pr + T_di)
            if Math.Abs(lambda_phi1 - lambda_phi) / lambda_phi >= delta then
                let delta1 = (lambda_phi1 - lambda_phi) / k2
                loop (lambda_phi1 - delta1) (it + 1)
            else T_k, T_pr, T_di, it, lambda_phi1
        let T_k, T_pr, T_di, it, lambda_phin = loop lambda_phi1 0
        let T_c = T_0 + T_r + T_k + T_pr + T_di
        let rho_pc = (T_0 + T_r) / T_c
        let rho_p = T_r / T_c
        let lambda = float n / T_c
        let rho_k = 2.0 * lambda * t_k
        let rho_pr = beta * lambda * (t_pr / float c)
        let rho_di = beta * lambda * p_di * t_di

        this.Rho_pc <- rho_pc
        this.Rho_p <- rho_p
        this.C_sr <- rho_pc * float n
        this.Rho_k <- rho_k
        this.Rho_pr <- rho_pr
        this.Rho_di <- rho_di
        this.T_c <- T_c
        this.T_r <- T_k + T_pr + T_di
        this.Lambda_phi1 <- lambda_phi1
        this.Lambda_phin <- lambda_phin
        this.It <- it
