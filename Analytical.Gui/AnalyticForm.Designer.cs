﻿namespace Analytical.Gui
{
    partial class AnalyticForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputGroupBox = new System.Windows.Forms.GroupBox();
            this.deltaNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.deltaLabel = new System.Windows.Forms.Label();
            this.K2NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.K2Label = new System.Windows.Forms.Label();
            this.K1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.K1Label = new System.Windows.Forms.Label();
            this.p_iNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.t_diNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.dNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.t_prNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.cNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.t_k2NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.t_k1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.T_rNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.T_0NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.nNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.p_iLabel = new System.Windows.Forms.Label();
            this.t_diLabel = new System.Windows.Forms.Label();
            this.dLabel = new System.Windows.Forms.Label();
            this.t_prLabel = new System.Windows.Forms.Label();
            this.cLabel = new System.Windows.Forms.Label();
            this.t_k2Label = new System.Windows.Forms.Label();
            this.t_k1Label = new System.Windows.Forms.Label();
            this.T_rLabel = new System.Windows.Forms.Label();
            this.T_0Label = new System.Windows.Forms.Label();
            this.nLabel = new System.Windows.Forms.Label();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultGroupBox = new System.Windows.Forms.GroupBox();
            this.resultRichTextBox = new System.Windows.Forms.RichTextBox();
            this.modelButton = new System.Windows.Forms.Button();
            this.inputGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deltaNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.K2NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.K1NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_iNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_diNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_prNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_k2NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_k1NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_rNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_0NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nNumericUpDown)).BeginInit();
            this.mainMenuStrip.SuspendLayout();
            this.resultGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputGroupBox
            // 
            this.inputGroupBox.Controls.Add(this.deltaNumericUpDown);
            this.inputGroupBox.Controls.Add(this.deltaLabel);
            this.inputGroupBox.Controls.Add(this.K2NumericUpDown);
            this.inputGroupBox.Controls.Add(this.K2Label);
            this.inputGroupBox.Controls.Add(this.K1NumericUpDown);
            this.inputGroupBox.Controls.Add(this.K1Label);
            this.inputGroupBox.Controls.Add(this.p_iNumericUpDown);
            this.inputGroupBox.Controls.Add(this.t_diNumericUpDown);
            this.inputGroupBox.Controls.Add(this.dNumericUpDown);
            this.inputGroupBox.Controls.Add(this.t_prNumericUpDown);
            this.inputGroupBox.Controls.Add(this.cNumericUpDown);
            this.inputGroupBox.Controls.Add(this.t_k2NumericUpDown);
            this.inputGroupBox.Controls.Add(this.t_k1NumericUpDown);
            this.inputGroupBox.Controls.Add(this.T_rNumericUpDown);
            this.inputGroupBox.Controls.Add(this.T_0NumericUpDown);
            this.inputGroupBox.Controls.Add(this.nNumericUpDown);
            this.inputGroupBox.Controls.Add(this.p_iLabel);
            this.inputGroupBox.Controls.Add(this.t_diLabel);
            this.inputGroupBox.Controls.Add(this.dLabel);
            this.inputGroupBox.Controls.Add(this.t_prLabel);
            this.inputGroupBox.Controls.Add(this.cLabel);
            this.inputGroupBox.Controls.Add(this.t_k2Label);
            this.inputGroupBox.Controls.Add(this.t_k1Label);
            this.inputGroupBox.Controls.Add(this.T_rLabel);
            this.inputGroupBox.Controls.Add(this.T_0Label);
            this.inputGroupBox.Controls.Add(this.nLabel);
            this.inputGroupBox.Location = new System.Drawing.Point(12, 27);
            this.inputGroupBox.Name = "inputGroupBox";
            this.inputGroupBox.Size = new System.Drawing.Size(458, 356);
            this.inputGroupBox.TabIndex = 0;
            this.inputGroupBox.TabStop = false;
            this.inputGroupBox.Text = "Входные параметры";
            // 
            // deltaNumericUpDown
            // 
            this.deltaNumericUpDown.DecimalPlaces = 6;
            this.deltaNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.deltaNumericUpDown.Location = new System.Drawing.Point(388, 326);
            this.deltaNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            this.deltaNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            393216});
            this.deltaNumericUpDown.Name = "deltaNumericUpDown";
            this.deltaNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.deltaNumericUpDown.TabIndex = 25;
            this.deltaNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            // 
            // deltaLabel
            // 
            this.deltaLabel.AutoSize = true;
            this.deltaLabel.Location = new System.Drawing.Point(7, 333);
            this.deltaLabel.Name = "deltaLabel";
            this.deltaLabel.Size = new System.Drawing.Size(32, 13);
            this.deltaLabel.TabIndex = 24;
            this.deltaLabel.Text = "Delta";
            // 
            // K2NumericUpDown
            // 
            this.K2NumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.K2NumericUpDown.Location = new System.Drawing.Point(388, 299);
            this.K2NumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.K2NumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.K2NumericUpDown.Name = "K2NumericUpDown";
            this.K2NumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.K2NumericUpDown.TabIndex = 23;
            this.K2NumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // K2Label
            // 
            this.K2Label.AutoSize = true;
            this.K2Label.Location = new System.Drawing.Point(7, 306);
            this.K2Label.Name = "K2Label";
            this.K2Label.Size = new System.Drawing.Size(20, 13);
            this.K2Label.TabIndex = 22;
            this.K2Label.Text = "K2";
            // 
            // K1NumericUpDown
            // 
            this.K1NumericUpDown.DecimalPlaces = 6;
            this.K1NumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.K1NumericUpDown.Location = new System.Drawing.Point(388, 271);
            this.K1NumericUpDown.Maximum = new decimal(new int[] {
            999995,
            0,
            0,
            393216});
            this.K1NumericUpDown.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            this.K1NumericUpDown.Name = "K1NumericUpDown";
            this.K1NumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.K1NumericUpDown.TabIndex = 21;
            this.K1NumericUpDown.Value = new decimal(new int[] {
            995,
            0,
            0,
            196608});
            // 
            // K1Label
            // 
            this.K1Label.AutoSize = true;
            this.K1Label.Location = new System.Drawing.Point(7, 278);
            this.K1Label.Name = "K1Label";
            this.K1Label.Size = new System.Drawing.Size(20, 13);
            this.K1Label.TabIndex = 20;
            this.K1Label.Text = "K1";
            // 
            // p_iNumericUpDown
            // 
            this.p_iNumericUpDown.DecimalPlaces = 2;
            this.p_iNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.p_iNumericUpDown.Location = new System.Drawing.Point(388, 243);
            this.p_iNumericUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.p_iNumericUpDown.Name = "p_iNumericUpDown";
            this.p_iNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.p_iNumericUpDown.TabIndex = 19;
            // 
            // t_diNumericUpDown
            // 
            this.t_diNumericUpDown.Location = new System.Drawing.Point(388, 217);
            this.t_diNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.t_diNumericUpDown.Name = "t_diNumericUpDown";
            this.t_diNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.t_diNumericUpDown.TabIndex = 18;
            this.t_diNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // dNumericUpDown
            // 
            this.dNumericUpDown.Location = new System.Drawing.Point(388, 191);
            this.dNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.dNumericUpDown.Name = "dNumericUpDown";
            this.dNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.dNumericUpDown.TabIndex = 17;
            this.dNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // t_prNumericUpDown
            // 
            this.t_prNumericUpDown.Location = new System.Drawing.Point(388, 165);
            this.t_prNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.t_prNumericUpDown.Name = "t_prNumericUpDown";
            this.t_prNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.t_prNumericUpDown.TabIndex = 16;
            this.t_prNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cNumericUpDown
            // 
            this.cNumericUpDown.Location = new System.Drawing.Point(388, 140);
            this.cNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.cNumericUpDown.Name = "cNumericUpDown";
            this.cNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.cNumericUpDown.TabIndex = 15;
            this.cNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // t_k2NumericUpDown
            // 
            this.t_k2NumericUpDown.Location = new System.Drawing.Point(388, 114);
            this.t_k2NumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.t_k2NumericUpDown.Name = "t_k2NumericUpDown";
            this.t_k2NumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.t_k2NumericUpDown.TabIndex = 14;
            this.t_k2NumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // t_k1NumericUpDown
            // 
            this.t_k1NumericUpDown.Location = new System.Drawing.Point(388, 89);
            this.t_k1NumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.t_k1NumericUpDown.Name = "t_k1NumericUpDown";
            this.t_k1NumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.t_k1NumericUpDown.TabIndex = 13;
            this.t_k1NumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // T_rNumericUpDown
            // 
            this.T_rNumericUpDown.Location = new System.Drawing.Point(388, 64);
            this.T_rNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.T_rNumericUpDown.Name = "T_rNumericUpDown";
            this.T_rNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.T_rNumericUpDown.TabIndex = 12;
            this.T_rNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // T_0NumericUpDown
            // 
            this.T_0NumericUpDown.Location = new System.Drawing.Point(388, 38);
            this.T_0NumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.T_0NumericUpDown.Name = "T_0NumericUpDown";
            this.T_0NumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.T_0NumericUpDown.TabIndex = 11;
            // 
            // nNumericUpDown
            // 
            this.nNumericUpDown.Location = new System.Drawing.Point(388, 13);
            this.nNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nNumericUpDown.Name = "nNumericUpDown";
            this.nNumericUpDown.Size = new System.Drawing.Size(59, 20);
            this.nNumericUpDown.TabIndex = 10;
            this.nNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // p_iLabel
            // 
            this.p_iLabel.AutoSize = true;
            this.p_iLabel.Location = new System.Drawing.Point(6, 250);
            this.p_iLabel.Name = "p_iLabel";
            this.p_iLabel.Size = new System.Drawing.Size(365, 13);
            this.p_iLabel.TabIndex = 9;
            this.p_iLabel.Text = "Вероятность обращения запроса к ЦП после обработки на диске (p_i)";
            // 
            // t_diLabel
            // 
            this.t_diLabel.AutoSize = true;
            this.t_diLabel.Location = new System.Drawing.Point(7, 224);
            this.t_diLabel.Name = "t_diLabel";
            this.t_diLabel.Size = new System.Drawing.Size(260, 13);
            this.t_diLabel.TabIndex = 8;
            this.t_diLabel.Text = "Среднее время обработки запроса на диске (t_di)";
            // 
            // dLabel
            // 
            this.dLabel.AutoSize = true;
            this.dLabel.Location = new System.Drawing.Point(7, 198);
            this.dLabel.Name = "dLabel";
            this.dLabel.Size = new System.Drawing.Size(120, 13);
            this.dLabel.TabIndex = 7;
            this.dLabel.Text = "Количество дисков (d)";
            // 
            // t_prLabel
            // 
            this.t_prLabel.AutoSize = true;
            this.t_prLabel.Location = new System.Drawing.Point(7, 172);
            this.t_prLabel.Name = "t_prLabel";
            this.t_prLabel.Size = new System.Drawing.Size(291, 13);
            this.t_prLabel.TabIndex = 6;
            this.t_prLabel.Text = "Среднее время обработки запроса на процессоре (t_pr)";
            // 
            // cLabel
            // 
            this.cLabel.AutoSize = true;
            this.cLabel.Location = new System.Drawing.Point(7, 147);
            this.cLabel.Name = "cLabel";
            this.cLabel.Size = new System.Drawing.Size(156, 13);
            this.cLabel.TabIndex = 5;
            this.cLabel.Text = "Колличество процессоров (c)";
            // 
            // t_k2Label
            // 
            this.t_k2Label.AutoSize = true;
            this.t_k2Label.Location = new System.Drawing.Point(7, 121);
            this.t_k2Label.Name = "t_k2Label";
            this.t_k2Label.Size = new System.Drawing.Size(360, 13);
            this.t_k2Label.TabIndex = 4;
            this.t_k2Label.Text = "Среднее время передачи через канал в обратном направлении (t_k2)";
            // 
            // t_k1Label
            // 
            this.t_k1Label.AutoSize = true;
            this.t_k1Label.Location = new System.Drawing.Point(7, 96);
            this.t_k1Label.Name = "t_k1Label";
            this.t_k1Label.Size = new System.Drawing.Size(351, 13);
            this.t_k1Label.TabIndex = 3;
            this.t_k1Label.Text = "Среднее время передачи через канал в прямом направлении (t_k1)";
            // 
            // T_rLabel
            // 
            this.T_rLabel.AutoSize = true;
            this.T_rLabel.Location = new System.Drawing.Point(7, 71);
            this.T_rLabel.Name = "T_rLabel";
            this.T_rLabel.Size = new System.Drawing.Size(266, 13);
            this.T_rLabel.TabIndex = 2;
            this.T_rLabel.Text = "Среднее время формирования запроса на PC (T_r)";
            // 
            // T_0Label
            // 
            this.T_0Label.AutoSize = true;
            this.T_0Label.Location = new System.Drawing.Point(7, 45);
            this.T_0Label.Name = "T_0Label";
            this.T_0Label.Size = new System.Drawing.Size(258, 13);
            this.T_0Label.TabIndex = 1;
            this.T_0Label.Text = "Среднее время дообработки запроса на PC (T_0)";
            // 
            // nLabel
            // 
            this.nLabel.AutoSize = true;
            this.nLabel.Location = new System.Drawing.Point(7, 20);
            this.nLabel.Name = "nLabel";
            this.nLabel.Size = new System.Drawing.Size(170, 13);
            this.nLabel.TabIndex = 0;
            this.nLabel.Text = "Количество рабочих станций (N)";
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(687, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveReportToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // saveReportToolStripMenuItem
            // 
            this.saveReportToolStripMenuItem.Name = "saveReportToolStripMenuItem";
            this.saveReportToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.saveReportToolStripMenuItem.Text = "Сохранить отчёт";
            this.saveReportToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(162, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.helpToolStripMenuItem.Text = "Помощь";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.contentsToolStripMenuItem.Text = "Условные обозначения";
            this.contentsToolStripMenuItem.Click += new System.EventHandler(this.ContentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(201, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.aboutToolStripMenuItem.Text = "О программе...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // resultGroupBox
            // 
            this.resultGroupBox.Controls.Add(this.resultRichTextBox);
            this.resultGroupBox.Location = new System.Drawing.Point(477, 28);
            this.resultGroupBox.Name = "resultGroupBox";
            this.resultGroupBox.Size = new System.Drawing.Size(200, 355);
            this.resultGroupBox.TabIndex = 2;
            this.resultGroupBox.TabStop = false;
            this.resultGroupBox.Text = "Результат";
            // 
            // resultRichTextBox
            // 
            this.resultRichTextBox.Location = new System.Drawing.Point(6, 19);
            this.resultRichTextBox.Name = "resultRichTextBox";
            this.resultRichTextBox.Size = new System.Drawing.Size(188, 326);
            this.resultRichTextBox.TabIndex = 0;
            this.resultRichTextBox.Text = "";
            // 
            // modelButton
            // 
            this.modelButton.Location = new System.Drawing.Point(304, 389);
            this.modelButton.Name = "modelButton";
            this.modelButton.Size = new System.Drawing.Size(94, 23);
            this.modelButton.TabIndex = 3;
            this.modelButton.Text = "Моделировать";
            this.modelButton.UseVisualStyleBackColor = true;
            this.modelButton.Click += new System.EventHandler(this.ModelButton_Click);
            // 
            // AnalyticForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 420);
            this.Controls.Add(this.modelButton);
            this.Controls.Add(this.resultGroupBox);
            this.Controls.Add(this.inputGroupBox);
            this.Controls.Add(this.mainMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "AnalyticForm";
            this.Text = "Аналитическая модель";
            this.inputGroupBox.ResumeLayout(false);
            this.inputGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deltaNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.K2NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.K1NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_iNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_diNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_prNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_k2NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_k1NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_rNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_0NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nNumericUpDown)).EndInit();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.resultGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox inputGroupBox;
        private System.Windows.Forms.NumericUpDown p_iNumericUpDown;
        private System.Windows.Forms.NumericUpDown t_diNumericUpDown;
        private System.Windows.Forms.NumericUpDown dNumericUpDown;
        private System.Windows.Forms.NumericUpDown t_prNumericUpDown;
        private System.Windows.Forms.NumericUpDown cNumericUpDown;
        private System.Windows.Forms.NumericUpDown t_k2NumericUpDown;
        private System.Windows.Forms.NumericUpDown t_k1NumericUpDown;
        private System.Windows.Forms.NumericUpDown T_rNumericUpDown;
        private System.Windows.Forms.NumericUpDown T_0NumericUpDown;
        private System.Windows.Forms.NumericUpDown nNumericUpDown;
        private System.Windows.Forms.Label p_iLabel;
        private System.Windows.Forms.Label t_diLabel;
        private System.Windows.Forms.Label dLabel;
        private System.Windows.Forms.Label t_prLabel;
        private System.Windows.Forms.Label cLabel;
        private System.Windows.Forms.Label t_k2Label;
        private System.Windows.Forms.Label t_k1Label;
        private System.Windows.Forms.Label T_rLabel;
        private System.Windows.Forms.Label T_0Label;
        private System.Windows.Forms.Label nLabel;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown deltaNumericUpDown;
        private System.Windows.Forms.Label deltaLabel;
        private System.Windows.Forms.NumericUpDown K2NumericUpDown;
        private System.Windows.Forms.Label K2Label;
        private System.Windows.Forms.NumericUpDown K1NumericUpDown;
        private System.Windows.Forms.Label K1Label;
        private System.Windows.Forms.GroupBox resultGroupBox;
        private System.Windows.Forms.RichTextBox resultRichTextBox;
        private System.Windows.Forms.ToolStripMenuItem saveReportToolStripMenuItem;
        private System.Windows.Forms.Button modelButton;
    }
}

