﻿using Analytical.Library;
using System;
using System.Text;
using System.Windows.Forms;

namespace Analytical.Gui
{
    public partial class AnalyticForm : Form
    {
        public AnalyticForm()
        {
            InitializeComponent();
        }

        private void ModelButton_Click(object sender, EventArgs e)
        {
            var n = Convert.ToInt32(nNumericUpDown.Value);
            var T_0 = Convert.ToDouble(T_0NumericUpDown.Value);
            var T_r = Convert.ToDouble(T_rNumericUpDown.Value);
            var t_k1 = Convert.ToDouble(t_k1NumericUpDown.Value);
            var t_k2 = Convert.ToDouble(t_k2NumericUpDown.Value);
            var c = Convert.ToInt32(cNumericUpDown.Value);
            var t_pr = Convert.ToDouble(t_prNumericUpDown.Value);
            var d = Convert.ToInt32(dNumericUpDown.Value);
            var t_di = Convert.ToDouble(t_diNumericUpDown.Value);
            var p_i = Convert.ToDouble(p_iNumericUpDown.Value);
            var k1 = Convert.ToDouble(K1NumericUpDown.Value);
            var k2 = Convert.ToDouble(K2NumericUpDown.Value);
            var delta = Convert.ToDouble(deltaNumericUpDown.Value);

            var model = new AnalyticalModel();
            model.Run(n, T_0, T_r, t_k1, t_k2, c, t_pr, d, t_di, p_i, k1, k2, delta);
            resultRichTextBox.Text = ExtractResultFromModel(model);
        }

        private string ExtractResultFromModel(AnalyticalModel model)
        {
            var stringBuilder = new StringBuilder();
            foreach (var property in model.GetType().GetProperties())
            {
                stringBuilder.AppendFormat($"{property.Name} = {property.GetValue(model, null)}\n");
            }
            return stringBuilder.ToString();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog(this);
        }

        private void ContentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ContentsForm().Show(this);
        }
    }
}
