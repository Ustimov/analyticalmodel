﻿namespace Analytical.Gui
{
    partial class ContentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Rho_pcLabel = new System.Windows.Forms.Label();
            this.Rho_pLabel = new System.Windows.Forms.Label();
            this.C_srLabel = new System.Windows.Forms.Label();
            this.Rho_kLabel = new System.Windows.Forms.Label();
            this.Rho_prLabel = new System.Windows.Forms.Label();
            this.Rho_diLabel = new System.Windows.Forms.Label();
            this.T_cLabel = new System.Windows.Forms.Label();
            this.T_rLabel = new System.Windows.Forms.Label();
            this.Lambda_phi1Label = new System.Windows.Forms.Label();
            this.Lambda_phinLabel = new System.Windows.Forms.Label();
            this.ItLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Rho_pcLabel
            // 
            this.Rho_pcLabel.AutoSize = true;
            this.Rho_pcLabel.Location = new System.Drawing.Point(13, 13);
            this.Rho_pcLabel.Name = "Rho_pcLabel";
            this.Rho_pcLabel.Size = new System.Drawing.Size(188, 13);
            this.Rho_pcLabel.TabIndex = 0;
            this.Rho_pcLabel.Text = "Rho_pc - загрузка рабочей станции";
            // 
            // Rho_pLabel
            // 
            this.Rho_pLabel.AutoSize = true;
            this.Rho_pLabel.Location = new System.Drawing.Point(13, 38);
            this.Rho_pLabel.Name = "Rho_pLabel";
            this.Rho_pLabel.Size = new System.Drawing.Size(256, 13);
            this.Rho_pLabel.TabIndex = 1;
            this.Rho_pLabel.Text = "Rho_p - загрузка пользователя рабочей станции";
            // 
            // C_srLabel
            // 
            this.C_srLabel.AutoSize = true;
            this.C_srLabel.Location = new System.Drawing.Point(13, 64);
            this.C_srLabel.Name = "C_srLabel";
            this.C_srLabel.Size = new System.Drawing.Size(223, 13);
            this.C_srLabel.TabIndex = 2;
            this.C_srLabel.Text = "C_sr - среднее количество работающих PC";
            // 
            // Rho_kLabel
            // 
            this.Rho_kLabel.AutoSize = true;
            this.Rho_kLabel.Location = new System.Drawing.Point(13, 88);
            this.Rho_kLabel.Name = "Rho_kLabel";
            this.Rho_kLabel.Size = new System.Drawing.Size(133, 13);
            this.Rho_kLabel.TabIndex = 3;
            this.Rho_kLabel.Text = "Rho_k - загрузка канала";
            // 
            // Rho_prLabel
            // 
            this.Rho_prLabel.AutoSize = true;
            this.Rho_prLabel.Location = new System.Drawing.Point(13, 113);
            this.Rho_prLabel.Name = "Rho_prLabel";
            this.Rho_prLabel.Size = new System.Drawing.Size(160, 13);
            this.Rho_prLabel.TabIndex = 4;
            this.Rho_prLabel.Text = "Rho_pr - загрузка процессора";
            // 
            // Rho_diLabel
            // 
            this.Rho_diLabel.AutoSize = true;
            this.Rho_diLabel.Location = new System.Drawing.Point(13, 138);
            this.Rho_diLabel.Name = "Rho_diLabel";
            this.Rho_diLabel.Size = new System.Drawing.Size(148, 13);
            this.Rho_diLabel.TabIndex = 5;
            this.Rho_diLabel.Text = "Rho_di - загрузка i-го диска";
            // 
            // T_cLabel
            // 
            this.T_cLabel.AutoSize = true;
            this.T_cLabel.Location = new System.Drawing.Point(13, 164);
            this.T_cLabel.Name = "T_cLabel";
            this.T_cLabel.Size = new System.Drawing.Size(193, 13);
            this.T_cLabel.TabIndex = 6;
            this.T_cLabel.Text = "T_c - среднее время цикла системы";
            // 
            // T_rLabel
            // 
            this.T_rLabel.AutoSize = true;
            this.T_rLabel.Location = new System.Drawing.Point(13, 189);
            this.T_rLabel.Name = "T_rLabel";
            this.T_rLabel.Size = new System.Drawing.Size(202, 13);
            this.T_rLabel.TabIndex = 7;
            this.T_rLabel.Text = "T_r - среднее время реакции системы";
            // 
            // Lambda_phi1Label
            // 
            this.Lambda_phi1Label.AutoSize = true;
            this.Lambda_phi1Label.Location = new System.Drawing.Point(13, 214);
            this.Lambda_phi1Label.Name = "Lambda_phi1Label";
            this.Lambda_phi1Label.Size = new System.Drawing.Size(302, 13);
            this.Lambda_phi1Label.TabIndex = 8;
            this.Lambda_phi1Label.Text = "Lambda_phi1 - начальная интенсивность фонового потока";
            // 
            // Lambda_phinLabel
            // 
            this.Lambda_phinLabel.AutoSize = true;
            this.Lambda_phinLabel.Location = new System.Drawing.Point(13, 241);
            this.Lambda_phinLabel.Name = "Lambda_phinLabel";
            this.Lambda_phinLabel.Size = new System.Drawing.Size(296, 13);
            this.Lambda_phinLabel.TabIndex = 9;
            this.Lambda_phinLabel.Text = "Lambda_phin - конечная интенсивность фонового потока";
            // 
            // ItLabel
            // 
            this.ItLabel.AutoSize = true;
            this.ItLabel.Location = new System.Drawing.Point(13, 266);
            this.ItLabel.Name = "ItLabel";
            this.ItLabel.Size = new System.Drawing.Size(130, 13);
            this.ItLabel.TabIndex = 10;
            this.ItLabel.Text = "It - количество итераций";
            // 
            // ContentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 296);
            this.Controls.Add(this.ItLabel);
            this.Controls.Add(this.Lambda_phinLabel);
            this.Controls.Add(this.Lambda_phi1Label);
            this.Controls.Add(this.T_rLabel);
            this.Controls.Add(this.T_cLabel);
            this.Controls.Add(this.Rho_diLabel);
            this.Controls.Add(this.Rho_prLabel);
            this.Controls.Add(this.Rho_kLabel);
            this.Controls.Add(this.C_srLabel);
            this.Controls.Add(this.Rho_pLabel);
            this.Controls.Add(this.Rho_pcLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ContentsForm";
            this.Text = "Условные обозначения";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Rho_pcLabel;
        private System.Windows.Forms.Label Rho_pLabel;
        private System.Windows.Forms.Label C_srLabel;
        private System.Windows.Forms.Label Rho_kLabel;
        private System.Windows.Forms.Label Rho_prLabel;
        private System.Windows.Forms.Label Rho_diLabel;
        private System.Windows.Forms.Label T_cLabel;
        private System.Windows.Forms.Label T_rLabel;
        private System.Windows.Forms.Label Lambda_phi1Label;
        private System.Windows.Forms.Label Lambda_phinLabel;
        private System.Windows.Forms.Label ItLabel;
    }
}